﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Runtime.Remoting;
using Cheats.Kazumi.Core;
using Cheats.Kazumi.Objects;
using Cheats.Kazumi.Utils;
using UnityEngine;
using Console = Cheats.Kazumi.Core.Console;

namespace Cheats.Kazumi.Handlers {
    public class ConsoleHandler : MonoBehaviour{
        public static Dictionary<String, Command> _Commands = new Dictionary<string, Command>();
        
        void Start() {
            Type CmdType = typeof(Command);
            foreach (Type type in typeof(Loader).Assembly.GetTypes()) {
                

                try {

                
                if (type.IsSubclassOf(CmdType)) {
                      Command command = (Command) Activator.CreateInstance(type);
                     _Commands.Add(command.cmdinfo.Cmd.ToLower(), command);
                    Core.Console.Send(LogType.Log, "Loaded command: "+command.cmdinfo.Cmd);
                }
                }
                catch (Exception e) {
                   Console.Send(LogType.Error, e.ToString());
                    throw;
                }
                
            }   
            _Commands["Help"].OnRun(new string[]{});
            //PhotonNetwork.ConnectToRegion(CloudRegionCode.eu, FindObjectOfType<MainMenu>().gameBuild);
            
        }

        public static void HandleInput(string cmd, string[] args) {
            if (_Commands.ContainsKey(cmd.ToLower())) {
                _Commands[cmd.ToLower()].OnRun(args);

                /*   if (command.cmdinfo.RequerdArguments.Count >= args.Length) {
                    Console.Send(LogType.Error, "Syntax error! missing required arguments!");
                    foreach (var VARIABLE in command.cmdinfo.RequerdArguments.Values) {
                        Console.Send(LogType.Assert, "[" + VARIABLE.Name + "]" + " - " + VARIABLE.About);
                    }

                } else {
                    Argument[] required_arguments = new Argument[]{};
                    foreach (Argument VARIABLE in command.cmdinfo.RequerdArguments.Values) {
                        for (int i = 1; i < args.Length; i++) {
                            
                            String argumentname = args[i].StartsWith("-") ? args[i].Replace("-", "") : args[i];
                            if (VARIABLE.Name.ToLower() == argumentname) {


                                if (VARIABLE.prefix_Required && !args[i].StartsWith("-")) {
                                    Console.Send(LogType.Error,
                                        "Argument: " + VARIABLE.Name + " Requires the prefix -");
                                    return;
                                }
                                else if () {
                                    
                                }
                                {
                                    
                            
                            
                        int IValue;
                        String SValue;
                        float FValue;
                        bool BValue;
                        double DValue;
            
                        switch (VARIABLE.type) {
                            case Argument.ArgumentType.Boolean: {
                                if (bool.TryParse()) {
                                    
                                }
                                break;
                            }
                            case Argument.ArgumentType.Double: {
                                break;
                            }
                            case Argument.ArgumentType.Float: {
                                break;
                            }
                            case Argument.ArgumentType.Integer: {
                                break;
                            }
                            case Argument.ArgumentType.String: {
                                break;
                            }
                        }
                        
                    } 
                    }
                        
                }                
                    }           
                }
                
                        
            }    
        }*/

            }
        }
    }
}