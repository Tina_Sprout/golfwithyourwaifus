﻿using System;
using System.Collections.Generic;
using System.Linq;
using Cheats.Kazumi.Handlers;
using Cheats.Kazumi.Objects;
using UnityEngine;
using Console = Cheats.Kazumi.Core.Console;

namespace Cheats.Kazumi.Commands {
    [CommandInfo(Cmd:"Help", About:"Prints help about a command")]
    public class HelpCmd  : Command{
        public override void OnRun(string[] args) {
            if (args.Length <= 0) {
                foreach (string name in ConsoleHandler._Commands.Keys) {
                    CommandInfo cmdinfo = ConsoleHandler._Commands[name].cmdinfo;
                    Console.Send(LogType.Log, "<color=cyan>"+name+"</color>: "+cmdinfo.About);
                    
                }
            }
        }
    }
}