﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Console = Cheats.Kazumi.Core.Console;

namespace Cheats.Kazumi.Objects {
    public abstract class Command {
        public CommandInfo cmdinfo;

        public Command() {
            cmdinfo = GetType().GetCustomAttributes(typeof(CommandInfo), true).FirstOrDefault() as CommandInfo;
        }
        
        abstract public void OnRun(string[] args);

 
    }
    
    [AttributeUsage(AttributeTargets.Class, Inherited = false, AllowMultiple = false)]
    public sealed class CommandInfo : Attribute {
        // See the attribute guidelines at 
        //  http://go.microsoft.com/fwlink/?LinkId=85236
        public readonly string Cmd;
        
        // TODO: Impliment argument system
        public readonly string About;
/*
        public readonly Dictionary<string,Argument> RequerdArguments = new Dictionary<string, Argument>();

        public readonly Dictionary<string,Argument> OptionalArguments = new Dictionary<string, Argument>();
        */
        // This is a positional argument
        public CommandInfo(string Cmd, String About/*, string[] arguments = null*/) {
            this.Cmd = Cmd;
            this.About = About;
            /*if(arguments != null)
            cforeach (string argument in arguments) {
                Argument Aargument = new Argument(argument);
                if (Aargument.Requierd) RequerdArguments.Add(Aargument.Name,Aargument); else OptionalArguments.Add(Aargument.Name,Aargument);
            }*/
        
        }

        public string PositionalString { get; private set; }

        // This is a named argument
        public int NamedInt { get; set; }
    }
    
}