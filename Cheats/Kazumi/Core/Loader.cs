﻿using Cheats.Kazumi.Handlers;
using UnityEngine;

namespace Cheats.Kazumi.Core {
    public class Loader : MonoBehaviour{
        
        public static void Load() {
            GameObject cheat = null;
            if (cheat == null) {
                cheat = new GameObject();
                cheat.AddComponent<Console>();
                cheat.AddComponent<ConsoleHandler>();
                
                
                UnityEngine.Object.DontDestroyOnLoad(cheat);
            }
        }
        
    }
}