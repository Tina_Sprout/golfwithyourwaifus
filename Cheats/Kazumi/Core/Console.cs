﻿using System;
using System.Collections.Generic;
using Cheats.Kazumi.Handlers;
using Cheats.Kazumi.Utils;
using UnityEngine;

namespace Cheats.Kazumi.Core {
    
    /**
        Credits: This is a console based of RainbowTech's RudeCMD which is based of Matthew Miner's console.
    **/

    public class Console : MonoBehaviour {

        Dictionary<LogType, Color> LogLevelColors = new Dictionary<LogType, Color>() {
            {LogType.Assert, config.LogLevel_Assert},
            {LogType.Error, config.LogLevel_Error},
            {LogType.Exception, config.LogLevel_Execption},
            {LogType.Log, config.LogLevel_Log},
            {LogType.Warning, config.LogLevel_Warning}
        };

        private static Vector2 scrollposition;

        static List<Log> history = new List<Log>();
        static string Cmd = String.Empty;

        struct Log {
            public string message;
            public LogType level;
        }
        
        Rect windowRect = new Rect(Screen.width - 705, 0, 700, 400);
        Rect titlebarRect = new Rect(0,0,10000,20);
        GUIStyle LabelColor = new GUIStyle();

        public static void Send(LogType level, string message) {
            history.Add(new Log() {message = message, level = level});

            if (history.Count >= config.Log_maxamount) {
                history.RemoveAt(0);
            }

            if (config.Log_AutoScroll)
                scrollposition.y = Mathf.Infinity;
        }

        void Start() {
            LabelColor.richText = true;
        }

        void OnEnable() {
            Application.logMessageReceived += HandleLog;
        }

        void OnDisable() {
            Application.logMessageReceived -= HandleLog;
        }

        void OnGUI() {
            if (config.Console_Show) {
                if (Event.current.isKey && Event.current.keyCode == config.Keybind_Console && config.Console_Show && GUIUtility.keyboardControl > 0) {
                    GUIUtility.keyboardControl = 0;
                    Cmd = "";
                    config.Console_Show = false;
                }
                windowRect = GUILayout.Window(420, windowRect, ConsoleWindow, "console");
            }
        }
        
        //todo: impliment the stacktrace with a button that toggels it on and off, i wanna add statcktrace to the log struct
        void HandleLog(string message, string stacktrace, LogType Level) {
            history.Add(new Log(){message = message, level = Level});
            if (history.Count >= config.Log_maxamount) history.RemoveAt(0);
            if (config.Log_AutoScroll) scrollposition.y = Mathf.Infinity;
        }

        void ConsoleWindow(int windowID) {
            scrollposition = GUILayout.BeginScrollView(scrollposition);
            for (int i = 0; i < history.Count; i++) {
                var log = history[i];

                LabelColor.normal.textColor = LogLevelColors[log.level];
                GUILayout.Label(log.message, LabelColor);
            }
            GUILayout.EndScrollView();
            GUILayout.BeginHorizontal();
            GUI.SetNextControlName(config.Console_InternalControlerName);
            Cmd = GUILayout.TextField(Cmd, GUILayout.ExpandWidth(true));

            if (Event.current.isKey && Event.current.keyCode == KeyCode.Return && GUI.GetNameOfFocusedControl() == config.Console_InternalControlerName) {
                GUIUtility.keyboardControl = 0;
                String[] args = Cmd.Split(' ');
                ConsoleHandler.HandleInput(args[0], args);
                Cmd = "";
            }
            config.Log_AutoScroll =
                GUILayout.Toggle(config.Log_AutoScroll, "Auto scroll", GUILayout.ExpandWidth(false));
            GUILayout.EndHorizontal();
            GUI.DragWindow(titlebarRect);
        }
    }
}